import React from 'react';

const ContactPage = () => (
    <div>
      <h1>Contact Us</h1>
      <p>Please feel free to contact me.</p>
      <p>Cel: +27 ** *** **60</p>
      <p>Email: *****@gmail.com</p>
    </div>
);

export default ContactPage;