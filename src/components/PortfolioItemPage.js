import React from 'react';

const PortfolioItemPage = (props) => (
    <div>
      <h1>A view in my world!!!</h1>
      <p>This page item with the id of {props.match.params.id}</p>
    </div>
);

export default PortfolioItemPage;